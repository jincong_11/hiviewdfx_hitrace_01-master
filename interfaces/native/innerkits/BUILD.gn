# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/hiviewdfx/hitrace/hitrace.gni")
import("//build/ohos.gni")

config("libhitrace_pub_config") {
  visibility = [ ":*" ]

  include_dirs = [ "include" ]
}

ohos_shared_library("libhitracechain") {
  public_configs = [ ":libhitrace_pub_config" ]

  deps = [ "../../../frameworks/native:hitracechain_source" ]

  external_deps = []
  if (defined(ohos_lite)) {
    external_deps += [ "hilog_lite:hilog_lite" ]
  } else {
    external_deps += [ "hilog:libhilog" ]
  }

  output_extension = "so"

  if (build_public_version) {
    install_enable = true
  } else {
    install_enable = false
  }

  innerapi_tags = [
    "chipsetsdk_indirect",
    "platformsdk",
  ]

  version_script = "libhitracechain.map"

  part_name = "hitrace"

  install_images = [
    "system",
    "updater",
  ]

  subsystem_name = "hiviewdfx"
}

config("hitrace_meter_config") {
  visibility = [ ":*" ]
  include_dirs = [ "include/hitrace_meter" ]
}

ohos_static_library("hitrace_inner") {
  include_dirs = [
    "include/hitrace_meter",
    "include",
    "../../../frameworks/include/",
  ]
  sources = [
    "src/hitrace_meter.cpp",
    "src/hitrace_meter_c.c",
    "src/hitrace_meter_wrapper.cpp",
  ]

  external_deps = [
    "bounds_checking_function:libsec_static",
    "init:libbeget_proxy",
    "init:libbegetutil",
  ]

  if (defined(ohos_lite)) {
    external_deps += [ "hilog_lite:hilog_lite" ]
  } else {
    external_deps += [ "hilog:libhilog" ]
  }

  part_name = "hitrace"
  subsystem_name = "hiviewdfx"
}

config("hitrace_dump_config") {
  visibility = [ ":*" ]
  include_dirs = [ "include" ]
}

ohos_shared_library("hitrace_dump") {
  include_dirs = [
    "./include",
    "../../../frameworks/include/",
  ]
  public_configs = [ ":hitrace_dump_config" ]
  sources = [
    "../../../frameworks/native/common_utils.cpp",
    "../../../frameworks/native/dynamic_buffer.cpp",
    "./src/hitrace_dump.cpp",
  ]

  deps = [ "../../../config:hitrace_utils" ]

  external_deps = [
    "bounds_checking_function:libsec_static",
    "cJSON:cjson",
    "init:libbeget_proxy",
    "init:libbegetutil",
  ]

  if (defined(ohos_lite)) {
    external_deps += [ "hilog_lite:hilog_lite" ]
  } else {
    external_deps += [ "hilog:libhilog" ]
  }
  version_script = "hitrace.map"
  innerapi_tags = [ "platformsdk" ]
  part_name = "hitrace"
  subsystem_name = "hiviewdfx"
}

ohos_shared_library("hitrace_meter") {
  public_configs = [ ":hitrace_meter_config" ]
  deps = [
    ":hitrace_etc",
    ":hitrace_inner",
    ":libhitracechain",
  ]

  external_deps = [ "bounds_checking_function:libsec_static" ]
  if (defined(ohos_lite)) {
    external_deps += [ "hilog_lite:hilog_lite" ]
  } else {
    external_deps += [ "hilog:libhilog" ]
  }
  version_script = "hitrace.map"
  output_extension = "so"

  innerapi_tags = [
    "chipsetsdk",
    "platformsdk",
    "sasdk",
  ]
  part_name = "hitrace"
  install_images = [
    "system",
    "updater",
  ]
  subsystem_name = "hiviewdfx"
}

ohos_prebuilt_etc("hitrace.para") {
  source = "hitrace.para"
  install_images = [
    "system",
    "updater",
  ]
  module_install_dir = "etc/param"
  part_name = "hitrace"
  subsystem_name = "hiviewdfx"
}

ohos_prebuilt_etc("hitrace.para.dac") {
  source = "hitrace.para.dac"
  install_images = [
    "system",
    "updater",
  ]
  module_install_dir = "etc/param"
  part_name = "hitrace"
  subsystem_name = "hiviewdfx"
}

group("hitrace_etc") {
  deps = [
    ":hitrace.para",
    ":hitrace.para.dac",
  ]
}
